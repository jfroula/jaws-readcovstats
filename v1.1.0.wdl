workflow read_cov_stats {

    File reads
    File contigs


    call get_read_cov_stats {
        input:  reads=reads,
                contigs=contigs
    }

}

### Task definitions
task get_read_cov_stats {
    File reads
    File contigs

    runtime {
        cluster: "cori"
        poolname: "medium"
    }
    command {
        shifter --image=jfroula/readcovstats:1.1.1 read_cov_stats.sh ${reads} ${contigs}
    }
    output {
        File contig_cov = "contigcoverage.tsv"
        File contig_amb = "contigs.fasta.amb"
        File contig_ann = "contigs.fasta.ann"
        File contig_bwt = "contigs.fasta.bwt"
        File contig_fai = "contigs.fasta.fai"
        File contig_pac = "contigs.fasta.pac"
        File contig_sa = "contigs.fasta.sa"
        File contigstats = "contigstats.txt"
        File coverage = "coverage.tsv"
        File mapped_bam = "mappedreads.bam"
        File mapped_bai = "mappedreads.bam.bai"
        File mapped_stats = "mappingstats.txt"
    }
}
