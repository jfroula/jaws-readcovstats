FROM continuumio/miniconda
RUN conda install -c conda-forge ruby==2.4.5
RUN conda install -y -c bioconda seqtk==1.3
RUN conda install -y -c bioconda bwa==0.7.17
RUN conda install -y -c bioconda samtools==1.9
COPY bin/ /usr/local/bin
